from flask import Flask, request

# create app
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return '''
            <form method="post">
                <input type="text" name="textin" />
                <input type="submit" value="Input Text" />
            </form>
            
        '''
        
    elif request.method == 'POST':
        expression = request.form.get('textin')
        return 'Your text: %s' % expression

# run app
if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port=4000)
